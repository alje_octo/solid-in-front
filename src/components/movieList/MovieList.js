import React, {useEffect, useReducer, useState} from "react";
import {Movie} from "../movie/Movie";
import {Col, Container, Row} from "react-bootstrap";

const initialState = {
    dataLoading: true
}

function reducer(state, action) {
    switch (action.type) {
        case 'LOADING':
            return {dataLoading: true};
        case 'LOADED':
            return {dataLoading: false};
        default:
            return state;
    }
}

export const MovieList = () => {
    const [movies, setMovies] = useState([])
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        dispatch({type:'LOADING'})

        fetch('http://localhost:8080/movies')
            .then(response => response.json())
            .then(json => {
                dispatch({type: 'LOADED'})
                setMovies(json)
            })
    }, [])

    return <>
        <Container>
            <Row>
                <Col>Movies ({state.dataLoading ? 'Loading':'Loaded'})</Col>
            </Row>
        </Container>
        <br/>
        <Container>
            <Row><Col>Name</Col><Col>Type</Col></Row>
        {
            movies.map(movie => {
                return <Row><Movie movie={movie}/></Row>
            })
        }
        </Container>
        </>

}
